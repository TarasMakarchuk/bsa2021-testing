import CartParser from './CartParser';
import cart from '../samples/cart.json';

let parser;
let csv;

beforeEach(() => {
	parser = new CartParser();

	csv = `Product name,Price,Quantity
					Mollis consequat,9.00,2
					Tvoluptatem,10.32,1
					Scelerisque lacinia,18.90,1
					Consectetur adipiscing,28.72,10
					Condimentum aliquet,13.90,1`;
});

describe('CartParser - unit tests', () => {

	describe('CartParser: parse - synchronously reads the contents of a CSV file and parses it into a JSON object', () => {
		it('returns a JSON object with cart items and total price', () => {
			const dataObject = parser.parse('C:\\Users\\User\\WebstormProjects\\Projects_bsa_2021\\bsa_hw12_testing_1\\BSA2021-Testing\\samples\\cart.csv');
			const data = Object.keys(dataObject);

			expect(data).toContain('items');
			expect(data).toContain('total');
		});
	});

	describe('CartParser: validate - returns an array of validation errors. If the array is empty, validation is successful', () => {
		it('should be an empty array', () => {
			const data = parser.validate(csv);

			expect(data).toEqual([]);
		});

		it('should be a header validation error', () => {
			const csvNoHeaders = `
					Mollis consequat,9.00,2
					Tvoluptatem,10.32,1
					Scelerisque lacinia,18.90,1
					Consectetur adipiscing,28.72,10
					Condimentum aliquet,13.90,1`;
			const data = parser.validate(csvNoHeaders);
			const result = [
				{
					"column": 0,
					"message": "Expected header to be named \"Product name\" but received Mollis consequat.",
					"row": 0,
					"type": "header",
				},
				{
					"column": 1,
					"message": "Expected header to be named \"Price\" but received 9.00.",
					"row": 0,
					"type": "header",
				},
				{
					"column": 2,
					"message": "Expected header to be named \"Quantity\" but received 2.",
					"row": 0,
					"type": "header",
				}
			];

			expect(data).toEqual(result);
		});

		it('should be an row validation errors', () => {
			const csvNoRows = `Product name,Price,Quantity
					9.00,2
					Tvoluptatem,10.32,1
					1
					Consectetur adipiscing,28.72,10
					Condimentum aliquet,13.90,1`;
			const result = [
				{
					"column": -1,
					"message": "Expected row to have 3 cells but received 2.",
					"row": 1,
					"type": "row",
				},
				{
					"column": -1,
					"message": "Expected row to have 3 cells but received 1.",
					"row": 3,
					"type": "row",
				}
			];
			const data = parser.validate(csvNoRows);

			expect(data).toEqual(result);
		});

		it('should cell an empty string validation errors', () => {
			const csvEmptyCell = `Product name,Price,Quantity
					Mollis consequat,9.00,""
					Tvoluptatem,10.32,1
					Scelerisque lacinia,18.90,1
					Consectetur adipiscing,28.72,10
					Condimentum aliquet,13.90,1`;

			const result = [
				{
			    "column": 2,
			    "message": "Expected cell to be a nonempty string but received  \"\"\"\".",
			    "row": 1,
			    "type": "cell",
			   }
			];

			const data = parser.validate(csvEmptyCell);
			expect(data).toEqual(result);
		});

		it('displaying an error when checking a cell for a negative number', () => {
			const csvNegativeNumber = `Product name,Price,Quantity
					Mollis consequat,-9.00,2
					Tvoluptatem,10.32,1
					Scelerisque lacinia,-18.90,1
					Consectetur adipiscing,28.72,10
					Condimentum aliquet,13.90,1`;
			const result = [
				{
					"column": 1,
					"message": "Expected cell to be a positive number but received \"-9.00\".",
					"row": 1,
					"type": "cell",
				},
				{
					"column": 1,
					"message": "Expected cell to be a positive number but received \"-18.90\".",
					"row": 3,
					"type": "cell",
				}
			];
			const data = parser.validate(csvNegativeNumber);

			expect(data).toEqual(result);
		});
	});

	describe('CartParser: readFile', () => {
		it('returning a UTF-8 encoded string', () => {
			const path = 'samples\\cart.csv';
			const data = parser.readFile(path);
			const result = 'string';

			expect(typeof data).toBe(result);
		});
	});

	describe('CartParser: parseLine - сonverts a line from a valid CSV file to a JSON object, which represents a cart item', () => {
		it('returning an object with keys from columns, keys and values from CSV', () => {
			const csvLine = `Mollis consequat,-9.00,2`;
			const data = parser.parseLine(csvLine);
			const result = {
				name: 'Mollis consequat',
				price: -9,
				quantity: 2,
				id: data.id
			};

			expect(data).toEqual(result);
		});
	});

	describe('CartParser: calcTotal', () => {
		it('calculate the total price of items added to the cart', () => {
			const data = parser.calcTotal(cart.items);
			const result = 348.32;

			expect(data).toBeDefined();
			expect(data).toBeCloseTo(result);
		});

		it('the total price is not less and not more than the specified range', () => {
			const data = parser.calcTotal(cart.items);

			expect(data).toBeDefined();
			expect(data).toBeGreaterThan(348.31);
			expect(data).toBeLessThan(348.33);
		});
	});

});


describe('CartParser - integration test', () => {
	// Add your integration test here.
});